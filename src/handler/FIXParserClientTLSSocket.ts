/*
 * fixparser
 * https://gitlab.com/logotype/fixparser.git
 *
 * Copyright 2019 Victor Norgren
 * Released under the MIT license
 */
import { Socket } from 'net';

import { EventEmitter } from 'events';
import { TLSSocket } from 'tls';
import FIXParser from '../FIXParser';
import Message from '../message/Message';
import FIXParserClientBase from './FIXParserClientBase';
import fs from 'fs';
import tls from 'tls';

export default class FIXParserClientTLSSocket extends FIXParserClientBase {
    private connected: boolean = false;
    protected tlsSocket: TLSSocket | null = null;

    constructor(eventEmitter: EventEmitter, parser: FIXParser) {
        super(eventEmitter, parser);
    }

    public connect() {
        console.log('Connecting...');
        this.socketTCP = new Socket();
        const options = {
            key: fs.readFileSync('private-key.pem'),
            cert: fs.readFileSync('public-cert.pem'),
            rejectUnauthorized: false
        };
        this.tlsSocket = tls.connect(+this.port!, this.host!, options, () => {
            console.log(`Connect though TLS socket to ${this.host}:${this.port}`);
            // Check if the authorization worked
            if (this.tlsSocket?.authorized) {
                console.log('Connection authorized by a Certificate Authority.');
            } else {
                console.log('Connection not authorized: ' + this.tlsSocket?.authorizationError)
            }
            this.eventEmitter!.emit('open');
            this.startHeartbeat();
        });

        this.tlsSocket!.setEncoding('ascii');

        this.tlsSocket!.on('data', (data: any) => {
            const messages: Message[] = this.fixParser!.parse(data.toString());
            let i = 0;
            for (i; i < messages.length; i++) {
                this.processMessage(messages[i]);
                this.eventEmitter!.emit('message', messages[i]);
            }
        });

        this.tlsSocket!.on('close', () => {
            console.log('Connection closed?'); // TODO why?
            this.connected = false;
            this.eventEmitter!.emit('close');
            this.stopHeartbeat();
        });

        this.tlsSocket!.on('error', (error) => {
            this.connected = false;
            console.log('error received', error);
            this.eventEmitter!.emit('error', error);
            this.stopHeartbeat();
        });

        this.tlsSocket!.on('timeout', () => {
            this.connected = false;
            this.eventEmitter!.emit('timeout');
            this.tlsSocket!.end();
            this.stopHeartbeat();
        });
    }

    public close() {
        if (this.tlsSocket) {
            this.tlsSocket!.destroy();
        } else {
            console.error(
                'FIXParserClientSocket: could not close socket, connection not open',
            );
        }
    }

    public send(message: Message) {
        if (this.tlsSocket?.authorized) {
            console.log('this.connected', this.tlsSocket?.authorized);
            console.log('message.encode()', message.encode('|'));
            try {
                this.fixParser!.setNextTargetMsgSeqNum(
                    this.fixParser!.getNextTargetMsgSeqNum() + 1,
                );
                this.tlsSocket!.write(message.encode());
            } catch (e) {
                console.log('e', e);
            }
        } else {
            console.error(
                'FIXParserClientSocket: could not send message, socket not open',
                message,
            );
        }
    }
}
