import FIXParser, {
    Field,
    Fields,
    Messages,
    Side,
    OrderTypes,
    HandlInst,
    TimeInForce,
    EncryptMethod,
} from '../src/FIXParser'; // from 'fixparser';

const fixParser = new FIXParser();
const SENDER = 'MDviewtest';
const TARGET = 'LMXBLM';

function sendLogon() {
    // Example: 8=FIX.4.4|9=102|35=A|34=1|49=FIXtest1|52=20120924-14:05:44.817|56=LMXBDM|553=FIXtest1|554=Password1|98=0|108=30|141=Y|10=011|
    // Actual: 8=FIX.4.4|9=106|35=A|34=1|49=MDviewtest|52=20200912-22:40:13.405|56=LMXBLM|141=Y|98=0|108=30|553=2096574767|554=password1|10=151|

    const logon = fixParser.createMessage(
        new Field(Fields.MsgType, Messages.Logon),
        new Field(Fields.MsgSeqNum, fixParser.getNextTargetMsgSeqNum()),
        new Field(Fields.SenderCompID, SENDER),
        new Field(Fields.SendingTime, fixParser.getTimestamp()),
        new Field(Fields.TargetCompID, TARGET),
        new Field(Fields.ResetSeqNumFlag, 'Y'),
        new Field(Fields.EncryptMethod, EncryptMethod.PEM),
        new Field(Fields.HeartBtInt, 30),
        new Field(Fields.Username, '2096574767'),
        new Field(Fields.Password, 'password1'),
    );
    const messages = fixParser.parse(logon.encode());
    console.log(
        'sending message',
        messages[0].description,
        messages[0].string.replace(/\x01/g, '|'),
    );
    fixParser.send(logon);
}
fixParser.connect({
    host: 'fix-md.lmaxtrader.com',
    port: 443,
    protocol: 'ssl',
    sender: SENDER,
    target: TARGET,
    fixVersion: 'FIX.4.4',
});

fixParser.on('open', () => {
    console.log('Open');

    sendLogon();

    // setInterval(() => {
    //     const order = fixParser.createMessage(
    //         new Field(Fields.MsgType, Messages.MarketDataRequest),
    //         new Field(Fields.MDReqID, fixParser.getNextTargetMsgSeqNum()),
    //         new Field(Fields.SubscriptionRequestType, '1'),
    //         new Field(Fields.MarketDepth, '0'),
    //         new Field(Fields.SenderCompID, SENDER),
    //     );
    //     const messages = fixParser.parse(order.encode());
    //     console.log(
    //         'sending message',
    //         messages[0].description,
    //         messages[0].string.replace(/\x01/g, '|'),
    //     );
    //     fixParser.send(order);
    // }, 1000);

    // setInterval(() => {
    //     const order = fixParser.createMessage(
    //         new Field(Fields.MsgType, Messages.NewOrderSingle),
    //         new Field(Fields.MsgSeqNum, fixParser.getNextTargetMsgSeqNum()),
    //         new Field(Fields.SenderCompID, SENDER),
    //         new Field(Fields.SendingTime, fixParser.getTimestamp()),
    //         new Field(Fields.TargetCompID, TARGET),
    //         new Field(Fields.ClOrdID, '11223344'),
    //         new Field(
    //             Fields.HandlInst,
    //             HandlInst.AutomatedExecutionNoIntervention,
    //         ),
    //         new Field(Fields.OrderQty, '123'),
    //         new Field(Fields.TransactTime, fixParser.getTimestamp()),
    //         new Field(Fields.OrdType, OrderTypes.Market),
    //         new Field(Fields.Side, Side.Buy),
    //         new Field(Fields.Symbol, '700.HK'),
    //         new Field(Fields.TimeInForce, TimeInForce.Day),
    //     );
    //     const messages = fixParser.parse(order.encode());
    //     console.log(
    //         'sending message',
    //         messages[0].description,
    //         messages[0].string.replace(/\x01/g, '|'),
    //     );
    //     fixParser.send(order);
    // }, 1500);
});
fixParser.on('message', (message) => {
    console.log('received message', message.description, message.string);
});
fixParser.on('close', () => {
    console.log('Disconnected');
});
